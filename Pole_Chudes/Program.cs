﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pole_Chudes
{
    class Program
    {
        static void Main(string[] args)
        {
            int countLetters;

            char[] user1;
            char[] user2;

            string hint;
            char currentLetter;
            bool hasCurrentLetter, hasStar;
            const char STAR_SYMBOL = '*';

            Console.WriteLine("Приветсвуем вас на капитал шоу Поле-Чудес!");
            Console.Write("Введите кол-во букв в вашем слове: ");
            countLetters = int.Parse(Console.ReadLine());

            user1 = new char[countLetters];
            user2 = new char[countLetters];

            Console.WriteLine("Введите загаданное слово по буквам.");

            for (int i = 0; i < countLetters; i++)
            {
                user1[i] = char.Parse(Console.ReadLine());
            }

            for (int i = 0; i < countLetters; i++)
            {
                user2[i] = STAR_SYMBOL;
            }

            Console.Write("Введите подсказку: ");
            hint = Console.ReadLine();

            do
            {
                hasStar = false;
                hasCurrentLetter = false;
                Console.Clear();

                Console.WriteLine(hint);

                for (int i = 0; i < countLetters; i++)
                {
                    Console.Write(user2[i]);
                }
                Console.WriteLine();

                Console.WriteLine("Введите букву: ");
                currentLetter = char.Parse(Console.ReadLine());
                
                for(int i=0; i<countLetters;i++)
                {
                    if(currentLetter==user1[i])
                    {
                        user2[i] = currentLetter;
                        hasCurrentLetter = true;
                    }
                }
                if(hasCurrentLetter==true)
                {
                    Console.WriteLine("Такая буква есть в этом слове!");
                }
                else
                {
                    Console.WriteLine("Такой буквы нет.");
                }
                Console.ReadKey();

                for(int i=0; i<countLetters;i++)
                {
                    if (user2[i]==STAR_SYMBOL)
                    {
                        hasStar = true;
                        break;
                    }
                }

            } while (hasStar == true);

            Console.Clear();

            Console.WriteLine("Вы угадали слово. Поздравляем!");

            for (int i = 0; i < countLetters; i++)
            {
                Console.Write(user2[i]);
            }



            Console.ReadKey();
        }


    }
}
